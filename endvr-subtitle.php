<?php
/**
 * Plugin Name: 	Endeavr - Subtitle Metabox
 * Plugin URI: 	http://endeavr.com
 * Description: 	Adds a Subtitle Metabox to Posts, Pages, and Custom Post Types
 * Version: 		5.0.0
 * Author: 		Endeavr Media (Jason Loftis / jLoft)
 * Author URI: 	http://endeavr.com
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * @package		ENDVR_Subtitle
 * @version		5.0.0
 * @since			1.0.0
 * @author		Endeavr Media <support@endeavr.com>
 * @copyright		Coppyright (c) 2013, Jason Loftis (jLOFT / Endeavr / ChurchAmp)
 * @link			http://endeavr.com
 * @license		http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 *
 * Code Sources:
 * http://wordpress.org/extend/plugins/subtitle-360/
 * http://wp.tutsplus.com/tutorials/plugins/how-to-create-custom-wordpress-writemeta-boxes/
 * http://wp.tutsplus.com/articles/tips-articles/quick-tip-using-wp_editor/
 * http://wordpress.org/support/topic/move-custom-meta-box-above-editor?replies=12
 * http://thomasgriffinmedia.com/blog/2011/07/how-to-automatically-add-meta-boxes-to-custom-post-types/
 */


add_action( 'add_meta_boxes', 'endvr_meta_box_add' );
function endvr_meta_box_add()
{
	$post_types = get_post_types();
	foreach ( $post_types as $post_type )
	add_meta_box( 'endvr_feature_subtitle', 'Sub Title', 'endvr_meta_box_cb', $post_type, 'normal', 'high' );
}

function endvr_meta_box_cb( $post ) {
	$values = get_post_custom( $post->ID );
	$text = isset( $values['endvr_feature_subtitle'] ) ? esc_attr( $values['endvr_feature_subtitle'][0] ) : '';
	wp_nonce_field( 'endvr_meta_box_nonce', 'meta_box_nonce' );
	?>
	<p>
		<label for="endvr_feature_subtitle"><?php endvr_subtitle_label(); ?></label><br>
		<input type="text" name="endvr_feature_subtitle" id="endvr_feature_subtitle" value="<?php echo $text; ?>" class="widefat" />
	</p>
	<?php

}

add_action( 'save_post', 'endvr_meta_box_save' );
function endvr_meta_box_save( $post_id )
{
	// Bail if we're doing an auto save
	if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) return;

	// if our nonce isn't there, or we can't verify it, bail
	if( !isset( $_POST['meta_box_nonce'] ) || !wp_verify_nonce( $_POST['meta_box_nonce'], 'endvr_meta_box_nonce' ) ) return;

	// if our current user can't edit this post, bail
	if( !current_user_can( 'edit_post' ) ) return;

	// now we can actually save the data
	$allowed = array(
		'a' => array( // on allow a tags
			'href' => array() // and those anchords can only have href attribute
		)
	);

	// Probably a good idea to make sure your data is set
	if( isset( $_POST['endvr_feature_subtitle'] ) )
		update_post_meta( $post_id, 'endvr_feature_subtitle', wp_kses( $_POST['endvr_feature_subtitle'], $allowed ) );
}

function the_subtitle() {
		global $post;
		echo '<span class="feature-subtitle">';
		echo get_post_meta($post->ID, 'endvr_feature_subtitle', true);
		echo '</span>';
}

function endvr_subtitle_label() {
	do_action('endvr_subtitle_label');
}
function endvr_subtitle_label_default() {
	echo 'Input a Subtitle for this Content (Optional)';
}
add_filter( 'endvr_subtitle_label', 'endvr_subtitle_label_default' );

// move the sub title above the post editor + assign a custom label to the Post Editor
// http://wordpress.org/support/topic/move-custom-meta-box-above-editor?replies=17
// http://software.troydesign.it/php/wordpress/move-wp-visual-editor.html
add_action( 'add_meta_boxes', 'endvr_add_meta_box_editor_postpage', 0 );
function endvr_add_meta_box_editor_postpage() {
     $screen = get_current_screen();
     if  ( 'post' == $screen->post_type | 'page' == $screen->post_type ) {
		global $_wp_post_type_features;
		foreach ($_wp_post_type_features as $type => &$features) {
			if (isset($features['editor']) && $features['editor']) {
				unset($features['editor']);
				add_meta_box(
					'endvr_editor_description_postpage',
					__('Content Editor'),
					'endvr_meta_box_editor_postpage',
					$type, 'normal', 'core'
				);
			}
		}
	}
	add_action( 'admin_head', 'endvr_admin_head_postpage'); //white background
}
function endvr_admin_head_postpage() {
	?>
	<style type="text/css">
		.wp-editor-container{background-color:#fff;}
	</style>
	<?php
}
function endvr_meta_box_editor_postpage( $post ) {
	echo '<div class="wp-editor-wrap">';
	wp_editor($post->post_content, 'content', array('dfw' => true, 'tabindex' => 1) );
	echo '</div>';
}