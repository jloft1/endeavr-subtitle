Endeavr - Subtitle Metabox Plugin

=== Plugin Code Source ===
Contributors: jLOFT / Endeavr
Tags: subtitle, metabox, page, post, custom post type
Requires at least: 3.5
Tested up to: 3.5.1
Stable tag: 2.0.0
License: http://www.gnu.org/licenses/old-licenses/gpl-2.0.html

This plugin creates an option to enter sub heading for pages and posts. You can display the sub title in your theme by using the <code><?php if (function_exists('the_subtitle')){ the_subtitle(); }?></code>

== Description ==

This plugin creates an option to enter sub heading for pages and posts. You can display the sub title in your theme by using the
<code><?php if (function_exists('the_subtitle')){ the_subtitle(); }?></code>

== Installation ==

1. Upload the entire `endvr.subtitle` folder to the `/wp-content/plugins/` directory.
2. Activate the plugin through the 'Plugins' menu in WordPress.
3. Now under pages and posts you can see option to enter sub title.

<strong>Template Tag</strong>
<code><?php if (function_exists('the_subtitle')){ the_subtitle(); }?></code>

== Frequently Asked Questions ==

= My Subtitle won't upload. What should I do? =

Maybe you forgot to add template code to the area you want to display subtitle.
<code><?php if (function_exists('the_subtitle')){ the_subtitle(); }?></code>

== Changelog ==

= 2.0.0 =
* Initial Release